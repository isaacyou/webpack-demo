var webpack = require('webpack');
module.exports = {
	entry: {
		vendor:['jquery','bootstrap','jquery-slimscroll','underscore'],//表示一个组
		app:__dirname+"/app/main.js",
		// vendor2:['bootstrap'],
	},
	output:{
		path:__dirname+"/public",
		//可以使用扩展的样式[name].js,会用entry的key代替name
		filename:'bouder.js'
	},
	devServer:{
		port:8080,
		contentBase:__dirname+"/public",
		colors:true,
		historyApiFallback:true,
		inline:true
	},
	module:{
		loaders:[{
            test: /\.js$/,
            exclude: /node_modules/,
            loader: 'babel-loader'
        },{
			test:/\.json$/,
			loader:'json'
		},{
			test:/\.css$/,
			loader:'style!css'
		}
		]
	},
	plugins:[
	    //导入jquery的控件，需要npm安装
		new webpack.ProvidePlugin({
			$:'jquery',
			jQuery:'jquery',
			"window.jQuery":'jquery',
			_: "underscore",	
		}),
		//将vendor打包成vendor.js文件，不写会直接打包到output指定文件
		new webpack.optimize.CommonsChunkPlugin("vendor", "vendor.js")
		//打包成一个太大时可以采用打包成两个,必须使output.filrname:'[name].js'类似的扩展的名字
		// new webpack.optimize.CommonsChunkPlugin({
		// 	names: ["vendor","vendor2"],
		// 	minChunks: Infinity
		// })
	],
	resolve:{
		//野方法导入jquery这样的需要使用requery导入到js中
		// alias:{
		// 	'jquery':__dirname+"/app/jquery.min.js"
		// }
	}
};
